package com.zipfile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import com.pojo.Mobile;
import com.pojo.Passport;
import com.pojo.Student;

public class ReadDataZip {
	
	public ArrayList<Student> studentdetails() {
		
		ArrayList<Student> s = new ArrayList<>();
		
	try {
		FileReader fr = new FileReader("E:\\A\\Studentdetails\\student.txt");
		BufferedReader br = new BufferedReader(fr);
		String str ;
		while((str=br.readLine())!=null) {
			String values[] = str.split(" ");
		Student st = new Student();
		st.setSid(Integer.parseInt(values[0]));
		st.setSname(values[1]);
		st.setSage(Integer.parseInt(values[2]));
		st.setSqul(values[3]);
		s.add(st);
		}
		
		
	}catch(Exception e) {
		e.printStackTrace();
	}
	return s;
	}
	
public ArrayList<Passport> passportdetails() {
		
		ArrayList<Passport> p = new ArrayList<>();
		
	try {
		FileReader fr = new FileReader("E:\\A\\Studentdetails\\passport.txt");
		BufferedReader br = new BufferedReader(fr);
		String str ;
		while((str=br.readLine())!=null) {
			String values[] = str.split(" ");
		Passport st = new Passport();
		st.setPid(Integer.parseInt(values[0]));
		st.setPnum(Integer.parseInt(values[1]));
		st.setPexpd(Integer.parseInt(values[2]));
	
		p.add(st);
		}
		
		
	}catch(Exception e) {
		e.printStackTrace();
	}
	return p;
	}


public ArrayList<Mobile> mobiledetails() {
	
	ArrayList<Mobile> s = new ArrayList<>();
	
try {
	FileReader fr = new FileReader("E:\\A\\Studentdetails\\mobile.txt");
	BufferedReader br = new BufferedReader(fr);
	String str ;
	while((str=br.readLine())!=null) {
		String values[] = str.split(" ");
	Mobile st = new Mobile();
	st.setMid(Integer.parseInt(values[0]));
	st.setMsim(values[1]);
	st.setMnum(Long.parseLong(values[2]));
	
	s.add(st);
	}
	
	
}catch(Exception e) {
	e.printStackTrace();
}
return s;
}
	
}
